from setuptools import setup, find_packages

setup(
    name='didactic',  # Required
    version='1.0.0',  # Required
    url='https://gitlab.com/JTreneL/cesky-jazyk-ditakticky-test',  # Optional
    author='JTrenel',  # Optional
    author_email='Janlenert@email.cz',  # Optional
    packages=find_packages(),  # Required
    entry_points={"console_scripts": ["didactic = didactic.didactic:main"]},
    #semafor = semafor.Semafor:main
    #jméno package = jméno_package.jméno_scriptu:metoda_která_se_má_spustit_na_začátku
    
)
