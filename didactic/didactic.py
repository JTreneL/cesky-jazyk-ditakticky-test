import time 
import sys

def colored(r, g, b, text):
    return "\033[38;2;{};{};{}m{} \033[38;2;255;255;255m".format(r, g, b, text)

class Question:
     def __init__(self, prompt, answer):
          self.prompt = prompt
          self.answer = answer

def main():
    while True:
        print(colored(0,100,200,"JAZYKOVÉ PROSTŘEDKY FIGUR A TROPŮ A K ČEMU SLOUŽÍ? v.1.0"))
        time.sleep(3)

        #DEFINICE JAZÝKOVÝCH PROSTŘEDKŮ OBECNÁ

        print("""\nJedná se o nepřímá pojmenování skutečnosti ! 
        zvláštní jazykové obraty či různé slovní hříčky !""")
        time.sleep(2)
        print("""\nJazykové prostředky nám přinášejí značné množství komplikací, 
        a to zejména ve chvílích, kdy se je učíme rozeznat 
        – ať už v českém jazyce či cizím jazyce.""")
        time.sleep(2)
        print("""\nKdyž někdo řekne, že jsme světlem jeho života, 
        musíme to umět správně dešifrovat a pochopit, 
        co tím měl básník na mysli.""")
        time.sleep(2)
        vyber_1 = input (colored(0,100,200,"\n Vyber si téma 1. FIGURY 2. TROPY 3. TEST Z JP 4.ODCHOD: "))
        if vyber_1 == "1":
            print(colored(0,200,50,"\n Zvolil sis FIGURY "))
            #TS
            print("\nJAK POZNÁME FIGURY?")
            #TS
            print("""\nS figurami se setkáme především (ne ale výlučně) v umělecké literatuře, převážně pak v poezii.
                Jedná se o literární prostředek založený hře s jazykem po jeho formální stránce.
                Často jsou to různá opakování, vypouštění hlásek/slov či např. změna slovosledu. 
                Obecně se dá říci, že figuru často poznáme na první pohled. 
                V tabulce níže uvádíme některé používané figury i s jejich vysvětlením.""")
                #TS
            print("\nFIGURY:")
            time.sleep(2)     
            print("aliterace = opakování stejných hlásek na začátcích slov.")
            time.sleep(2)
            print("anafora = opakování slov na začátku veršů.")
            time.sleep(2)
            print("epifora = opakování slov na konci veršů.")
            time.sleep(2)
            print("epizeuxis = opakování slov v jednom verši.")
            time.sleep(2)
            print("inverze = změněný slovosled.")
            time.sleep(2)
            print("řečnická otázka = otázka, na níž nečekáme odpověď.")
            time.sleep(2)
            print("eufemismus = přikrášlení.")
            time.sleep(2)
            print("dysfemismus = zhrubění.")
            time.sleep(2)
            print("elipsa = vypouštění slov, která si lze domyslet.")
            time.sleep(30)
            print(colored(200, 0, 0,"\nKONEC PŘEDNÁŠKY NA TÉMA FIGURY"))
            
        elif vyber_1 == "2":
            print(colored(0,200,50,"Zvolil sis TROPY"))
            #TS
            print("\nJAK POZNÁME TROPY?")
            #TS
            print("""\nTropy jsou o něco složitější nežli figury, a to zejména z toho důvodu, že je na první pohled poznáme jen zřídka. 
            Nemají totiž pokaždé stejný tvar a vycházejí z významové složky jazyka.
            V Českém jazyce si jako příklad uvedeme “Zlom vaz,”.""")
            #TS
            print("\nTROPY:")
            time.sleep(2)     
            print("metafora = přenesení významu na základě vnější skutečnosti.")
            time.sleep(2) 
            print("metonymie = přenesení významu na základě vnitřní souvislosti.")
            time.sleep(2)
            print("personifikace = polidštění.")
            time.sleep(2)
            print("přirovnání =	přirovnání k něčemu, často za použití „jako“.")
            time.sleep(2)
            print("epiteton = básnický přívlastek.")
            time.sleep(2)
            print("oxymóron = nesmyslné spojení slov.")
            time.sleep(2)
            print("ironie = opak toho, co si skutečně myslíme.")
            time.sleep(30)
            print(colored(200, 0, 0,"\nKONEC PŘEDNÁŠKY NA TÉMA TROPY."))
        
        elif vyber_1 == "3":
            print(colored(200,200,10,"Zvolil sis TEST."))
            main(questions)
            exit1 = input("Stiskni cokoliv pro odchod")
            break
        
        elif vyber_1 == "4":
            print(colored(200,200,10,"ODCHOD."))
            break
        
        else:
            print(colored(200, 0, 0,"\nMusíš si zvolit mezi možnostmi 1 až 4."))

question_prompts = [
     "Co je za jazykový styl: Mrtvého cit?\n(a)Oxymorón:\n(b)Personifikace:\n(c)Disfemismus:", #0
     
     "Co je za jazykový styl: Odešel do večných lovišt?\n(a)Ironie: \n(b)Metonymie: \n(c)Eufemismus:",#1
     
     "Co je za jazykový styl: Nákladní lodě spaly.\n(a)Hyperbola: \n(b)Personifikace: \n(c)Antiteze:",#2
     
     "Co je za jazykový styl: To bych mohl inzerovat sto let, že se mně ztratil pes. Dvě stě let, tři sta let!\n(a)Hyperbola: \n(b)Přirovnání,: \n(c)Disfemismus:",#3
     
     "Co je za jazykový styl: Ó matko má, v světlo proměněná!\n(a)Antiteze: \n(b)Apostrofa: \n(c)Rytmus:",#4
     
     "Co je za jazykový styl: Nad ouvalem sova houká.\n(a)Aliteracee: \n(b)Onomatopoie: \n(c)Epizeuxis:",#5
     
     "Co je za jazykový styl: Nade mnou kolo – kůl – kostlivec – lebka bledá.\n(a)Rytmus \n(b)Antiteze \n(c)Aliterace:",#6
     
     "Co je za jazykový styl: A ten shon a ruch a vřava.\n(a)Aliterace \n(b)Epizeuxis \n(c)Eufemismus:",#7
     
     "Co je za jazykový styl: Pojďme, pojďme, ale zticha.\n(a)Onomatopoie: \n(b)Epizeuxis: \n(c)Hyperbola:",#8
     
     "Co je za jazykový styl: Jsou dvě místo.\n(a)Elipsa: \n(b)Personifikace: \n(c)Oxymorón:",#9
     
     "Co je za jazykový styl: Vyslýchá ho policie.\n(a)Synekdocha: \n(b)Epizeuxis: \n(c)Eufemismus:",#10
     
     "Co je za jazykový styl: Počítačová myš?\n(a)Metonymie: \n(b)Metafora: \n(c)Apostrofa:",#11
     
     "Co je za jazykový styl: Měl oči, jako jasné nebe.\n(a)Personifikace: \n(b)Epizeuxis \n(c)Přirovnání:",#12
     
     "Co je za jazykový styl: Chcípl jako pes.\n(a)Anekdocha: \n(b)Přirovnání: \n(c)Disfemismus:",#13
]
questions = [
     Question(question_prompts[0], "a"),
     Question(question_prompts[1], "c"),
     Question(question_prompts[2], "b"),
     Question(question_prompts[3], "a"),
     Question(question_prompts[4], "b"),
     Question(question_prompts[5], "b"),
     Question(question_prompts[6], "c"),
     Question(question_prompts[7], "c"),
     Question(question_prompts[8], "b"),
     Question(question_prompts[9], "c"),
     Question(question_prompts[10], "a"),
     Question(question_prompts[11], "b"),
     Question(question_prompts[12], "c"),
     Question(question_prompts[13], "c"),
]
def quiz(questions):
     score = 0
     for question in questions:
          answer = input(question.prompt)
          if answer == question.answer:
               score += 1
     print("Získal si", score, "z ", len(questions))


if __name__ == "__main__":
    sys.exit(main())
